import re, sys, time, prettytable, pygal
from telnetlib import Telnet
from host import openvpn_config

def info (openvpn_mgm):
    try:
        print ("Connecting to:",openvpn_mgm["host"])
        with Telnet(
            host = (openvpn_mgm["host"]),
            port = (openvpn_mgm["port"]),
        ) as tn:
            print ("Connection establish")

            try:
                tn.write(b"status\n")
                status = tn.read_very_eager().decode("utf-8")
            except:
                print ("Command status error.")
                
            try:
                USER_INFO = {}
                for i in status.split("\n"):
                    if (re.search('[A-Z]', i)) or (re.search('(?:[0-9a-fA-F]:?){12}', i)) or (re.match('10', i)):
                        pass
                    elif i:
                        cname, raddress, breceived, bsent, csince = i.split(",")
                        USER_INFO["cname"] = cname
                        USER_INFO["raddress"] = raddress.split(":")[0]
                        USER_INFO["breceived"] = breceived
                        USER_INFO["bsent"] = bsent
                        USER_INFO["csince"] = csince
            except:
                print ("Parsing error")

    except:
        print ("Connection Error")
    
    try:
        return USER_INFO
    except:
        return print("Something wrong: Functio info ERROR")

def consoleOut (user):
    try:
        table = prettytable.PrettyTable()
        table.field_names = user.keys()
        table.add_rows(
            [
                user.values(),
            ]
        )
        print (f"{table}")
    except:
        print ("Empty users: function consoleOut")

def graficBar (stats):
    bar_chart = pygal.HorizontalStackedBar()
    bar_chart.title = "Bytes Received-Sent"
    bar_chart.x_labels = map(str, range(15))

    bar_chart.add("".join(list(stats.get("cname"))), [int(stats.get("bsent"))])
    print(stats.get("cname"))

    bar_chart.render_to_file("bar_chart.svg")

def main():
    USER_INFO = info(openvpn_config["openvpn_mgm2"])
    consoleOut(USER_INFO)
    graficBar(USER_INFO)

if __name__ == '__main__':
    main()
